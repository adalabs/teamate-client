import GPS

import os
import random
import sys
import time

global pin
global session
global user
global proc
global project
global kind
global ed
global over
global server
global file
global token
global force_Project_Load
global git_Initial_Is_In_Branch_Head
global git_Initial_Branch_Commit
global git_Initial_Branch_Name
global git_Command_Status
global git_Command_Output
global git_Commit_Change_Occurs
global last_location_changed_msg
global last_text_selected_msg
global start_line
global end_line
global triggered
global timeout_handler
global process_id
global prev_request
global hooks_are_setted
global pid_received

# ENV
#   - TEAMATE_BASE_PATH
#
pid_received = False
hooks_are_setted = False
pin = ""
session = ""
user = ""
token = ""
ed = None
over = None
file = None
force_Project_Load = False
server = "https://adalabs.com:4141"
git_Initial_Is_In_Branch_Head = False
git_Initial_Branch_Commit = ""
git_Initial_Branch_Name = ""
git_Command_Status = False
git_Command_Output = ""
git_Commit_Change_Occurs = False
last_location_changed_msg = ""
last_text_selected_msg = ""
start_line = 0
end_line = 0
triggered = False
requests = []
process_id = ""
prev_request = ""
ready_flag = False
send_flag = False
kind = ""

#############################################################
#


def git_Commit_Exists(ref):
    global git_Initial_Branch_Commit
    if ref != git_Initial_Branch_Commit:
        cmd = GPS.Process("git cat-file -e " + ref, before_kill=on_kill_git, remote_server="Execution_Server", on_exit=on_exit_git)
        cmd.wait()
        return git_Command_Status

#############################################################
#


def git_Checkout(ref):
    global git_Initial_Branch_Commit
    global force_Project_Load
    global git_Command_Status
    global git_Command_Output
    global git_Commit_Change_Occurs
    
    if ref != git_Initial_Branch_Commit:
        if git_Commit_Exists(ref) == True:
            cmd = GPS.Process("git checkout " + ref, before_kill=on_kill_git, remote_server="Execution_Server", on_exit=on_exit_git)
            cmd.wait()
            if git_Command_Status:
                force_Project_Load = True
                git_Commit_Change_Occurs = True
            else:
                sys.stdout.write("\n\x1B[31m [FAIL] git checkout failure, session '" + session + "' ref '" + ref + "'  '" + git_Command_Output + "' \x1B[0m\n")
        else:
            sys.stdout.write("\n\x1B[31m [FAIL] git checkout failure, session '" + session + "' unknown ref '" + ref + "' \x1B[0m\n")
            
#############################################################
#


def git_Init():
    global git_Initial_Is_In_Branch_Head
    global git_Initial_Branch_Commit
    global git_Initial_Branch_Name
    global git_Command_Status
    global git_Command_Output
    
    # git_Initial_Branch_Commit
    cmd = GPS.Process("git rev-parse HEAD", before_kill=on_kill_git, remote_server="Execution_Server", on_exit=on_exit_git)
    cmd.wait()
    if git_Command_Status == True:
        git_Initial_Branch_Commit = git_Command_Output
    else:
        git_Initial_Branch_Commit = ""
        
    # git_Initial_Is_In_Branch_Head
    # git_Initial_Branch_Name    
    cmd = GPS.Process("git symbolic-ref --short HEAD", before_kill=on_kill_git, remote_server="Execution_Server", on_exit=on_exit_git)
    cmd.wait()
    if git_Command_Status:
        git_Initial_Is_In_Branch_Head = True
        git_Initial_Branch_Name = git_Command_Output
    else:
        git_Initial_Is_In_Branch_Head = False
        git_Initial_Branch_Name = ""

    log("git_Initial_Branch_Commit: " + git_Initial_Branch_Commit)
    log("git_Initial_Is_In_Branch_Head: " + str(git_Initial_Is_In_Branch_Head))
    log("git_Initial_Branch_Name: " + git_Initial_Branch_Name)

#############################################################
#


def log(message):
    print message
#    file.write(message + "\n")
#    file.flush()


#############################################################
#

def send(trigger, params):
    global session
    global process_id
    global prev_request
                                                                                                                                
    request = "/forge/teamate/records/" + trigger + "?" + "session=" + session + "&" + params
    if request != prev_request:
        prev_request = request
        #print "[send] " + request
        cmd = GPS.Process("teamate-send.sh \"" + request + "\" /proc/" + process_id + "/fd/0", before_kill=on_kill_send, remote_server="Execution_Server", on_exit=on_exit_send)

#############################################################
#
        

def assess_text_selected(timeout):
    global start_line
    global end_line
    global triggered
    
    if start_line != 0:
        on_text_selected("on_text_selected", start_line, end_line)

#############################################################
#
        

def on_context_changed(hook, context):
    global start_line
    global end_line
    global triggered
    global timeout_handler
    global last_text_selected_msg
    
    if not context.file() is None:
        location = context.location()
        if triggered:
            timeout_handler.remove()
            triggered = False
        start_line = 0
        last_text_selected_msg = ""
        if not location is None: 
            on_location_changed("on_location_changed", context.file(), location.line(), location.column())
        else:
            on_location_changed("on_location_changed", context.file(), 1, 1)
    if context.start_line() != 0:
        start_line = context.start_line()
        end_line = context.end_line()
        if not triggered:
            triggered = True
            timeout_handler = GPS.Timeout(1000, assess_text_selected)
        else:
            timeout_handler.remove()
            timeout_handler = GPS.Timeout(1000, assess_text_selected)

#############################################################
#
        

def on_location_changed(hook, file, line, column):
    global last_location_changed_msg
    msg = "file=" + file.name().split('/')[-1] + "&line=" + str(line) + "&column=" + str(column)
    if last_location_changed_msg != msg:
        send("location_changed", msg)
    last_location_changed_msg = msg

#############################################################
#
    

def on_commit_changed(hook, ref):
    send("commit_changed", "commit=" + ref)


#############################################################
#
    

def on_project_changed(hook, file):
    global project
    
    project = file.name()
    send("project_changed", "name=" + file.name().split(os.environ.get('TEAMATE_BASE_PATH', ''))[1])

#############################################################
#
    

def on_text_selected(hook, start, stop):
    global last_text_selected_msg
    msg = "start=" + str(start) + "&stop=" + str(stop)
    if last_text_selected_msg != msg:
        send("text_selected", msg)
    last_text_selected_msg = msg

#############################################################
#


def on_session_ended(hook, session):
    send("session_ended", "")
 
#############################################################
#


def reset_overlay():
    global ed
    global over
    if ed is not None and over is not None:
        try:
            GPS.EditorBuffer.remove_overlay(ed, over)
        except:
            pass
    ed = None
    over = None
    
    
#############################################################
#


def process(command):
    global ed
    global over
    global file
    global force_Project_Load
    global process_id
    global kind
    global proc
    global git_Initial_Branch_Commit
    global hooks_are_setted
    global pid_received
    global prev_request
    global session
    
    command = command.rstrip('\n\r ')
    #log("processing:" + command)
    if kind == "lead":
        if len(command.split("pid;")) == 2:
            sys.stdout.write ("\n\x1B[32m [TEAMATE] Session: " + session + " Pin: " + pin + " Kind: " + kind + " \x1B[0m\n")
            process_id = command.split("pid;")[1].strip()
            pid_received = True
        elif "ready;" in command:
            if pid_received and hooks_are_setted == False:
                # Now that we have the PID, and the ready command, we can send the notifications
                on_commit_changed("on_commit_changed", git_Initial_Branch_Commit)
                on_project_changed("on_project_changed", GPS.Project.root().file())
                on_location_changed("on_location_changed", GPS.current_context().file(), GPS.current_context().location().line(), GPS.current_context().location().column())
                GPS.Hook("project_changing").add(on_project_changed)
                GPS.Hook("context_changed").add(on_context_changed)
                hooks_are_setted = True
        elif len(command.split("switch;")) == 2:
            origin = command.split("switch;")[1].strip()
            if GPS.MDI.yes_no_dialog("Do you want to let '" + origin + "' lead the session ?"):
                send("session_ended", "switch=accept")
                prev_request = ""
                GPS.Hook("project_changing").remove(on_project_changed)
                GPS.Hook("context_changed").remove(on_context_changed)
                hooks_are_setted = False
                #kind = ""
                #cmd = GPS.Process("kill -9 " + process_id, remote_server="Execution_Server")
                kind = "follow"
                GPS.Editor.register_highlighting("teamate-" + session, "Dark Slate Blue",speedbar=True)
                #proc = GPS.Process("teamate-create.sh " + user + " " + server + " " + session + " " + pin + " follow", regexp='^cmd*',on_match=on_match, before_kill=on_kill, remote_server="Execution_Server", on_exit=on_exit, strip_cr=True)        
            else:
                send("session_ended", "switch=refuse")
                prev_request = ""
        elif len(command.split("session_ended;")) == 2:
            # Now that the session_ended has been sent to the server, we can kill the client
            if proc != None:
                proc.interrupt()
            kind = ""
    elif kind == "follow":
        if len(command.split("pid;")) == 2:
            process_id = command.split("pid;")[1].strip()
            sys.stdout.write ("\n\x1B[32m [TEAMATE] Session: " + session + " Pin: " + pin + " Kind: " + kind + " \x1B[0m\n")
        elif len(command.split("project_changed;")) == 2:
            project = command.split("project_changed;")[1]
            if project != "":
                if not GPS.Project.root().file() is None:
                    if GPS.Project.root().file().name() != os.environ.get('TEAMATE_BASE_PATH') + project:
                        log("loading '" + os.environ.get('TEAMATE_BASE_PATH') + project + "'")
                        GPS.Project.load(os.environ.get('TEAMATE_BASE_PATH') + project)
                    elif force_Project_Load == True:
                        log("loading '" + os.environ.get('TEAMATE_BASE_PATH') + project + "'")
                        GPS.Project.load(os.environ.get('TEAMATE_BASE_PATH') + project)
                else:
                    log("loading '" + os.environ.get('TEAMATE_BASE_PATH') + project + "'")
                    GPS.Project.load(os.environ.get('TEAMATE_BASE_PATH') + project)
            force_Project_Load = False
        elif len(command.split("location_changed;")) == 2:
            reset_overlay()
            content = command.split("location_changed;")[1].split(";")
            name = content[0]
            file = name
            line = int(content[1])
            column = int(content[2])
            gps_file = GPS.File(name)
            GPS.Locations.add("teamate-" + session, gps_file, line, column, message="reviewed",highlight="teamate", length=1)
            ed = GPS.EditorBuffer.get(gps_file, open=True)
            view = ed.current_view()
            GPS.MDI.get_by_child(view).raise_window()
            loc = ed.at(line, column)
            view.goto(loc)
            view.center(loc)
        elif len(command.split("text_selected;")) == 2:
            if file is not None:
                reset_overlay()
                content = command.split("text_selected;")[1].split(";")
                start_line = int(content[0])
                end_line = int(content[1])
                center = (start_line + end_line) / 2
                buf = GPS.EditorBuffer.get()
                over = buf.create_overlay("text_selected")
                over.set_property("paragraph-background", "Dark Slate Blue")
                ed = GPS.EditorBuffer.get(GPS.File(file))
                loc = ed.at(center, 1)
                view = ed.current_view()
                view.center(loc)
                start = ed.at(start_line, 1)
                end = ed.at(end_line, 1)
                GPS.EditorBuffer.apply_overlay(ed, over, start, end)
        elif len(command.split("commit_changed;")) == 2:
            sessionCommit = command.split("commit_changed;")[1]
            git_Checkout(sessionCommit)
        elif "switch-already_in_progress;" in command:
            GPS.MDI.dialog ("a lead switch process is already in progress, please try again later")
        elif len(command.split("switch-accepted;")) == 2:
            origin = command.split("switch-accepted;")[1]
            #kind = ""
            #cmd = GPS.Process("kill -9 " + process_id, remote_server="Execution_Server")
            GPS.MDI.dialog ("'" + origin + "' gave you the leadership of session '" + session + "'")
            GPS.Hook("project_changing").remove(on_project_changed)
            GPS.Hook("context_changed").remove(on_context_changed)
            #cmd.wait()
            reset_overlay()
            kind = "lead"
            hooks_are_setted = False
            pid_received = True
            process("cmd;ready;")
            #proc = GPS.Process("teamate-create.sh " + user + " " + server + " " + session + " " + pin + " lead", regexp='^cmd*',on_match=on_match, before_kill=on_kill, remote_server="Execution_Server", on_exit=on_exit, strip_cr=True)
        elif len(command.split("switch-refused;")) == 2:
            origin = command.split("switch-refused;")[1]
            GPS.MDI.dialog ("'" + origin + "' refused your lead on  '" + session + "' !")
            


#############################################################
#


def radomString():
    pin1 = random.randint(0, 9)
    pin2 = random.randint(0, 9)
    pin3 = random.randint(0, 9)
    pin4 = random.randint(0, 9)
    return str(pin1) + str(pin2) + str(pin3) + str(pin4)


#############################################################
#


def get_session_user_and_pin(initiate=True):
    global server
    global session
    global user
    global pin
    
    if server == "":
        server = "https://adalabs.com:4141"
    if user == "":
        user = os.environ.get('USER', 'Love')
    if session == "":
        if initiate:
            pin1 = random.randint(0, 9)
            pin2 = random.randint(0, 9)
            pin3 = random.randint(0, 9)
            pin4 = random.randint(0, 9)
            information = GPS.MDI.input_dialog("Teamate session", "user=" + user, "server=" + server, "session=review" + radomString(), "pin=" + radomString())
        else:
            information = GPS.MDI.input_dialog("Teamate session","user=" + user, "server=" + server, "session=", "pin=")
    else:
        information = GPS.MDI.input_dialog("Teamate session", "user=" + user, "server=" + server, "session=" + session, "pin=" + pin)
    if information:
        user, server, session, pin = information
        return True
    else:
        return False


#############################################################
#


def lead_session():
    global pin
    global session
    global user
    global kind
    global proc
    global prev_request

    if kind == "":
        print "creating session"
        GPS.Hook("project_changing").remove(on_project_changed)
        GPS.Hook("context_changed").remove(on_context_changed)
        if get_session_user_and_pin() == True:
            kind = "lead"
            git_Init()
            # TODO generate session and pin numbers
        
            # HOOK MANAGEMENT
            # file_saved(name, file)
            # GPS.Hook("location_changed").add(on_location_changed)
            # location_changed hook is not need as we got the information using context_changed hook
            #
            
            proc = GPS.Process("teamate-create.sh " + user + " " + server + " " + session + " " + pin + " lead", regexp='^cmd*',on_match=on_match, before_kill=on_kill, remote_server="Execution_Server", on_exit=on_exit, strip_cr=True)
            ## for debug only
            #process("cmd;pid;27182")
            #process("cmd;ready;")
    elif kind == "lead":
        GPS.MDI.dialog ("you are already leading session " + session + ", end it first to lead another one")
    elif kind == "follow":
        send("session_ended", "switch=lead&user=" + user)
        prev_request = ""
            
        
        
#############################################################
#


def on_match(self, matched, unmatched):
    # log("matched: " + matched + " unmatched: " + unmatched)
    process(matched + unmatched)
    
#############################################################
#


def on_kill_send(self, remaining_output):
    sys.stdout.write("\n\x1B[31m [FAIL] send failure \x1B[0m\n")    

#############################################################
#


def on_kill(self, remaining_output):
    global kind
    global pid_received
    global hooks_are_setted
    
    if kink == "follow":
        sys.stdout.write("\n\x1B[31m [FAIL] follow session '" + session + "' killed \x1B[0m\n")
    elif kind == "lead":
        sys.stdout.write("\n\x1B[31m [FAIL] create session '" + session + "' killed \x1B[0m\n")
        pid_received = False
        hooks_are_setted = False
        GPS.Hook("project_changing").remove(on_project_changed)
        GPS.Hook("context_changed").remove(on_context_changed)
    kind = ""


#############################################################
#
def get_exit_reason(status):
    reason = ""
    if status == 3:
        reason = "invalid pin"
    elif status == 4:
        reason = "invalid session"
    elif status == 9:
        reason = "leader conflict"    
    elif status == 12:
        reason = "session creation quota exceeded"
    elif status == 255:
        reason = "unexpected error"
    elif status == 100:
        reason = "bad client command line"
    elif status == 142:
        reason = "network error"
    else:
        reason = "unknown " + str(status)
    return reason

def on_exit(self, status, remaining_output):
    global session
    global kind
    global session
    global proc
    global kind
    global process_id
    global pid_received
    global hooks_are_setted
    
    if kind == "follow":
        if status != 0 and status != 137:
            feedback = "follow session '" + session + "' failure '" + get_exit_reason(status) + "', please follow again"
            sys.stdout.write("\n\x1B[31m [FAIL] " + feedback + " \x1B[0m\n")
            GPS.MDI.dialog ("error during " + feedback)
            kind = ""
    elif kind == "lead":
        pid_received = False
        hooks_are_setted = False
        if status != 0 and kind != "":
            if status != 137:
                feedback = "lead session '" + session + "' failure '" + get_exit_reason(status) + "' , please create your session again"
                sys.stdout.write("\n\x1B[31m [FAIL] " + feedback + " \x1B[0m\n")
                GPS.MDI.dialog ("error during " + feedback)
                kind = ""
            GPS.Hook("project_changing").remove(on_project_changed)
            GPS.Hook("context_changed").remove(on_context_changed)
        
#############################################################
#


def on_exit_send(self, status, remaining_output):
    global session
    global send_flag
    global proc
    global kind

    send_flag = True
    if status != 0 and kind != "":
        sys.stdout.write("\n\x1B[31m [FAIL] send  '" + session + "' failure \x1B[0m\n")
        if kind == "lead":
            GPS.MDI.dialog ("communication error during send, please create your session again")
        kind = ""
        GPS.Hook("project_changing").remove(on_project_changed)
        GPS.Hook("context_changed").remove(on_context_changed)
        cmd = GPS.Process("kill -9 " + process_id, remote_server="Execution_Server")

#############################################################
#


def on_exit_git(self, status, remaining_output):
    global git_Command_Status
    global git_Command_Output
    git_Command_Status = (status == 0)
    git_Command_Output = remaining_output

#############################################################
#


def on_kill_git(self, remaining_output):
    global git_Command_Status
    global git_Command_Output
    git_Command_Status = False
    git_Command_Output = remaining_output    

#############################################################
#


def follow_session():
    global kind
    global proc
    global pin
    global session
    global user
    global server
    global process_id
    global prev_request
    
    if kind == "":
        if get_session_user_and_pin(initiate=False) == True:
            kind = "follow"
            git_Init()
            GPS.Editor.register_highlighting("teamate-" + session, "Dark Slate Blue",speedbar=True)
            proc = GPS.Process("teamate-create.sh " + user + " " + server + " " + session + " " + pin + " follow", regexp='^cmd*',on_match=on_match, before_kill=on_kill, remote_server="Execution_Server", on_exit=on_exit, strip_cr=True)            
    elif kind == "follow":
        GPS.MDI.dialog ("you are already following session " + session + ", end it first to follow another one")
    elif kind == "lead":
        # lead.end
        GPS.Hook("project_changing").remove(on_project_changed)
        GPS.Hook("context_changed").remove(on_context_changed)
        send("session_ended", "switch=follow")
        prev_request = ""
        #kind = ""
        #cmd = GPS.Process("kill -9 " + process_id, remote_server="Execution_Server")
        GPS.MDI.dialog ("you will now be following session '" + session + "'")
        #cmd.wait()
        kind = "follow"
        GPS.Editor.register_highlighting("teamate-" + session, "Dark Slate Blue",speedbar=True)
        #proc = GPS.Process("teamate-create.sh " + user + " " + server + " " + session + " " + pin + " follow", regexp='^cmd*',on_match=on_match, before_kill=on_kill, remote_server="Execution_Server", on_exit=on_exit, strip_cr=True)        
        
#############################################################
#
        

def end_session():
    global proc
    global kind
    global session
    global git_Initial_Is_In_Branch_Head
    global git_Initial_Branch_Commit
    global git_Initial_Branch_Name
    global git_Commit_Change_Occurs
    global process_id
    global hooks_are_setted
    
    sys.stdout.write("\n\x1B[32m [TEAMATE] end session '" + session + "' \x1B[0m\n")
    if kind == "lead":
        GPS.Hook("project_changing").remove(on_project_changed)
        GPS.Hook("context_changed").remove(on_context_changed)
        on_session_ended("session_ended", session)
        cmd = GPS.Process("kill -9 " + process_id, remote_server="Execution_Server")
    elif kind == "follow":
        if git_Commit_Change_Occurs:
            if git_Initial_Is_In_Branch_Head:
                git_Checkout(git_Initial_Branch_Name)
            else:
                git_Checkout(git_Initial_Branch_Commit)
        cmd = GPS.Process("kill -9 " + process_id, remote_server="Execution_Server")
        reset_overlay()
    hooks_are_setted = False
    kind = ""


#############################################################

GPS.parse_xml("""
 <action name = "Lead_Teamate_Session">
      <filter module = "Source_editor"/>
      <shell lang="python">teamate.lead_session()</shell>
</action>

 <action name = "Follow_Teamate_Session">
      <filter module = "Source_editor"/>
      <shell lang="python">teamate.follow_session()</shell>
</action>

 <action name = "End_Teamate_Session">
      <filter module = "Source_editor"/>
      <shell lang="python">teamate.end_session()</shell>
</action>

<contextual action = "Lead_Teamate_Session">
  <Title>Teamate/Lead session</Title>
</contextual>

<contextual action = "Follow_Teamate_Session">
  <Title>Teamate/Follow session</Title>
</contextual>

<contextual action = "End_Teamate_Session">
  <Title>Teamate/End session</Title>
</contextual>

""")


if os.environ.get('TEAMATE_BASE_PATH', '') == "":
    sys.stdout.write("\n\x1B[31m Please set TEAMATE_BASE_PATH environment variable and restart GPS in order to use AdaForge Teamate \x1B[0m\n")
