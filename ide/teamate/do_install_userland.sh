#!/bin/bash
# Script to install morpheus plugin in userland
#
INSTALLATION_PATH=~/.gnatstudio/plug-ins
#INSTALLATION_PATH=~/.gps/plug-ins
SOURCE_PATH=gps
RM=/bin/rm
CP=/bin/cp
MV=/bin/mv

$RM -rf $INSTALLATION_PATH/*

$CP $SOURCE_PATH/teamate.py $INSTALLATION_PATH/
$MV $INSTALLATION_PATH/teamate.py $INSTALLATION_PATH/teamate_dev.py

#Replace all occurences of adalabs by adalabs_dev in all python files
#
#rpl -i "adalabs." "adalabs_dev." $INSTALLATION_PATH/teamate_dev.py
rpl -i "teamate." "teamate_dev." $INSTALLATION_PATH/teamate_dev.py
rpl -i "adalabs/" "adalabs_dev/" $INSTALLATION_PATH/teamate_dev.py
find $INSTALLATION_PATH/adalabs_dev/ -name "*.py" -exec rpl -i "adalabs." "adalabs_dev." {} \;
