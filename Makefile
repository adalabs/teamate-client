PREFIX?=$(dir $(shell which gnatls))..
OWNER?=root
GROUP?=root
GPS_PLUGINS_DIR?=share/gps/plug-ins/
BIN_DIR?=bin/
DESTDIR?=

build:clean
	make -C client build-shared
clean:
	make -C client clean

deep-clean:clean
	rm -rf client/objects/*
	rm -rf client/binaries/*

install:build
	sudo mkdir -p $(DESTDIR)/$(PREFIX)/$(BIN_DIR)
	sudo mkdir -p $(DESTDIR)/$(PREFIX)/$(GPS_PLUGINS_DIR)/
	cd ide/teamate/gps ; tar -c ./  | sudo tar -x -C $(DESTDIR)/$(PREFIX)/$(GPS_PLUGINS_DIR)/
	sudo chown $(OWNER).$(GROUP) $(DESTDIR)/$(PREFIX)/$(GPS_PLUGINS_DIR)/teamate.py
	sudo chmod 644 $(DESTDIR)/$(PREFIX)/$(GPS_PLUGINS_DIR)/teamate.py
	sudo install -m 0755 -o $(OWNER) -g $(GROUP) client/binaries/teamate-client $(DESTDIR)/$(PREFIX)/$(BIN_DIR)
	sudo install -m 0755 -o $(OWNER) -g $(GROUP) client/scripts/teamate-create.sh $(DESTDIR)/$(PREFIX)/$(BIN_DIR)
	sudo install -m 0755 -o $(OWNER) -g $(GROUP) client/scripts/teamate-send.sh $(DESTDIR)/$(PREFIX)/$(BIN_DIR)

uninstall:
	sudo rm -rf $(DESTDIR)/$(PREFIX)/$(GPS_PLUGINS_DIR)/teamate.py
	sudo rm -rf $(DESTDIR)/$(PREFIX)/$(BIN_DIR)/teamate-client
	sudo rm -rf $(DESTDIR)/$(PREFIX)/$(BIN_DIR)/teamate-create.sh
	sudo rm -rf $(DESTDIR)/$(PREFIX)/$(BIN_DIR)/teamate-send.sh
