[[_TOC_]]

# 1. About teamate-client #

[![Teamate client trailer](http://img.youtube.com/vi/uu5_hbZKMXI/0.jpg)](http://www.youtube.com/watch?v=uu5_hbZKMXI "Teamate client trailer")

Teamate-client (GPLv3) aims to bring friendly & secure remote collaborative Software Engineering practises to GNAT Studio, 
using the appropriate IDE semantic level. 

Teamate together with your favourite voice chat software (Mumble, Jitsi, Pidgin, ...) aims to become your best remote 
programming friends.

During remote peer reviews, absolutely no source code are transferred through the network, 
but only project metadata thus greatly reducing source code exposure;

 * Git repository and commit references
 * Project file names
 * Source code file names, and cursor locations
 * Teamate session management
 
However, when remote pair programming feature will be available, only the delta source code added live by developers 
will be transferred through the network, and additional End-to-end encryption layer could be used. 

Teamate Client consists of;

* A GNAT Studio Python plugin `teamate.py`
   * Manage the necessary GNAT Studio hooks integration
   * Manage the teamate session (followers and leaders)
* An Ada web socket client `teamate-client`
   * HTTPS Web socket session to the Teamate server (current freely available server is `adalabs.com`)
   * Multi-tasked protected message queues between entities
* Linux specific bash scripts `teamate-create.sh teamate-send.sh`
   * Link between the Python plugin and the Ada web socket client to avoid any lag at GNAT Studio level is achieved using `/proc/<pid>/fd/0` pipe hack

Thanks to all the heroes contributing to the COTS used by `teamate-client`

# 2. Partial client-only release #

teamate-client is released ahead of schedule in order to ease remote peer reviews during COVID-19 confinement rules. 
teamate-server will be released at a later stage.

Our deepest sympathies to the Ada community people affected by the pandemic. 
 
# 3. Installation #

* Build dependencies are;
   * GNAT Community 2019
   * Ada Web Server
   * Bash shell
* teamate-client will be installed in the same root folder where the `gnatls` utility is found
* `sudo` command will be used to copy the files to destination, and set corresponding ownership and permission

```shell
cd <path-to>/teamate-client
make install
```

# 4. Uninstall #

* `sudo` command will be used to remove the installed files (in the same root folder where the `gnatls` utility is found)

```shell
cd <path-to>/teamate-client
make uninstall
```

# 5. Using Teamate Client #

##  5.1 Requirements ##

1. Prior to launching GNAT Studio, define the `TEAMATE_BASE_PATH` environment variable to your root working directory. 
For instance, set `TEAMATE_BASE_PATH` to `/home/lace/workspace/` as this folder contains all the team git projects

2. The project to be reviewed shall be available on the local git of each participant. The git commit reference will be shared to the
participants and necessary git commands will be launched for the project to view the same git commit reference. 

##  5.2 Lead a peer review ##

1. Open GNAT Studio source contextual menu by right clicking on a source content or an empty buffer

2. Select `Teamate > Lead session`

3. Fill the session parameters

| Parameter     | Default                  | Description                            |
| ------------- | ------------------------ | -------------------------------------- |
| user          | $USER                    | Username                               |
| server        | https://adalabs.com:4141 | Teamate server instance                |
| session       | Session unique name      | Random name                            |
| pin           | Session PIN              | Random PIN mandatory to join a session |

4. Click `Ok`

##  5.3 Follow a peer review ##
 
1. Open GNAT Studio source contextual menu by right clicking on a source content or an empty buffer

1. Select `Teamate > Follow session`

2. Fill the session parameters

| Parameter     | Default                  | Description                                 |
| ------------- | ------------------------ | ------------------------------------------- |
| user          | `$USER` value            | Username                                    |
| server        | https://adalabs.com:4141 | Teamate server instance                     |
| session       | Session unique name      | Name provided by the Teamate Session Leader |
| pin           | Session PIN              | PIN provided by the Teamate Session Leader  |

4. Click `Ok`

Once you join the session, a specific `teamate-<session>` category will be added in GNAT Studio location view, 
which allows you to make your peer review persistent.

##  5.4 Switch from Follower to Leader during a peer review ##

During a session, you can request to take the lead on a session by doing

1. Open GNAT Studio source contextual menu by right clicking on a source content or an empty buffer

2. Select  `Teamate > Lead session`

The current session leader will be informed, can accept or refuse your request, and you will be informed accordingly.

##  5.5 End a session ##

To end a session;

1. Open GNAT Studio source contextual menu by right clicking on a source content or an empty buffer

2. Select `Teamate > End session`

# 6. Limitations #

* Only the peer review feature is currently available
* teamate-client has been only validated on 
   * `amd64` CPU architecture
   * `GNU/Linux` Operating System
   * `GNAT Community 2019` IDE
* Contact us for further support, specific files are:
   * Process pipe using `/proc/<pid>/fd/0`
      * `client/scripts/teamate-create.sh`
      * `client/scripts/teamate-send.sh`
      * `ide/teamate/gps/teamate.py`
   * GNAT Studio plugin
      * `ide/teamate/gps/teamate.py`   
   * Web Socket data structure endianness
      * `client/sources/aws-client-extras.adb`
* Only a limited number of simultaneous connexions will be available on the server

# 7. Development #

* Adjust the `$ADALABS_ROOT_PATH` environment variable in the `env.sh` file according to your context
```shell
cd <path-to>/teamate-client
make install
```
* Launch the `env.sh` file
```shell
. ./env.sh
```
* Deploy the local version of the GNAT Studio plugin in `~/.gps/plug-ins`
```shell
cd ide/teamate
./do_install_userland.sh
```
* Relaunch all GNAT Studio instances after updating the teamate-client plugin

## 7.1 Teamate session protocol - Clients to server ##

Protocol for client to server are as follow:

`/forge/teamate/records/<trigger>?[<parameter>=<value>[&<parameter>=<value>]]`

| Trigger            | Parameter       | Value       |  Description                         |
| ------------------ | --------------- | ----------- | ------------------------------------ |           
| `location_changed` | `session`       | String      | session name                         |
|                    | `file`          | String      | location file                        |
|                    | `line`          | Positive    | location column                      |
|                    | `column`        | Positive    | location column                      |
| `commit_changed`   | `session`       | String      | session name                         |
|                    | `commit`        | String      | commit reference                     | 
| `project_changed`  | `session`       | String      | session name                         |
|                    | `name`          | String      | project relative name                | 
| `text_selected`    | `session`       | String      | session name                         |
|                    | `start`         | Positive    | selection start line                 |
|                    | `end`           | Positive    | selection end line                   |
| `session_ended`    | `session`       | String      | session name                         |
|                    | `switch`        | Enumeration | `lead`, `follow`, `accept`, `refuse` |

## 7.2 Teamate session protocol - Server to clients ##

Protocol for server to client are as follow:

`cmd;<trigger>;[<parameter>[;<parameter>]]`

| Command                                       | Example                                                       | Description                                     |
| --------------------------------------------- | ------------------------------------------------------------- | ----------------------------------------------- |
| `cmd;pid;<pid>`                               | `cmd;pid; 29215`                                              | Client pid                                      |
| `cmd;commit_changed;<commit-reference>`       | `cmd;commit_changed;b3af852b0fd49e5376fcba95d2fc776ad41f5208` | Session commit reference to be used             |
| `cmd;project_changed;<gpr-project>`           | `cmd;project_changed;/adaforge/server/factory.gpr`            | Session project to be loaded                    | 
| `cmd;location_changed;<file>;<line>;<column>` | `cmd;project_changed;factory.ads; 10; 1`                      | Session project to be loaded                    | 
| `cmd;end;`                                    | `cmd;end;`                                                    | Session ended                                   |
| `cmd;ready;`                                  | `cmd;ready;`                                                  | Client is ready                                 |
| `cmd;ack;`                                    | `cmd;ack;`                                                    | Acknowledge                                     |
| `cmd;switch;<user>`                           | `cmd;switch;Lace`                                             | Leadership request initiated by `Lace` follower |
| `cmd;switch-refused;<username>`               | `cmd;switch-refused;Love`                                     | Leadership transfer refused by `Love` leader)   |
| `cmd;switch-accepted;<username>`              | `cmd;switch-accepted;Love`                                    | Leadership transfer accepted by `Love` leader)  |
| `cmd;switch-already_in_progress;`             | `cmd;switch-already_in_progress;`                             | Leadership transfer refused by teamate-server   |

## 7.3 teamate-client exit status codes ##

`usage: teamate-client <user> https://<server>:<port> <session> <pin> <lead|follow> [--debug]`

| Exit status | Description                    |
| ----------- | ------------------------------ |
| `100`       | Command line error             |  
| `142`       | Network error                  |
| `255`       | Unexpected error               |
| `403`       | Invalid PIN                    |
| `404`       | Invalid session                |
| `409`       | Leader conflict                |
| `412`       | Session creation quota reached |


# 8. Roadmap #

Roadmap will be listening to early users needs. However, here is part of our backlog;

* Specific GNAT Studio cursor color when cursor has been trigger by the session leader
* Introduce co-leader capability, so that multiple co-leaders and leader can collectively lead a session
* Leader and co-leaders to propagate code manipulation
* Leader and co-leaders to propagate IDE level command (build/clean/run/...)
* End-to-end encryption
* Windows Operating System Port
* Other CPU architecture Port
* Session Chat view integrated to GNAT Studio
  * user to session chat flow
  * user to user chat flow

Contributions and feedbacks are most welcome
