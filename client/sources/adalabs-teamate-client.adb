--                              TEAMATE                                     --
--                                                                          --
--                     Copyright (C) 2018-2020, AdaLabs Ltd                 --
--                                                                          --
--  This library is free software;  you can redistribute it and/or modify   --
--  it under terms of the  GNU General Public License  as published by the  --
--  Free Software  Foundation;  either version 3,  or (at your  option) any --
--  later version. This library is distributed in the hope that it will be  --
--  useful, but WITHOUT ANY WARRANTY;  without even the implied warranty of --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--  You should have received a copy of the GNU General Public License and   --
--  a copy of the GCC Runtime Library Exception along with this program;    --
--  see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see   --
--  <http://www.gnu.org/licenses/>.                                         --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------
--
with Ada.Command_Line,
     Ada.Containers.Indefinite_Vectors,
     Ada.Exceptions,
     Ada.Strings.Unbounded,
     Ada.Synchronous_Task_Control,
     Ada.Text_IO;

use Ada.Strings.Unbounded;

with GNAT.OS_Lib;

with AWS.Client.Extras,
     AWS.NET;

procedure AdaLabs.Teamate.Client
is
   type Message_Kinds is (Local,
                          Remote);

   type Message_Type is record
      Kind    : Message_Kinds;
      Content : Unbounded_String;
   end record;

   Ws        : AWS.Client.HTTP_Connection;
   Message   : Message_Type;
   SO_Local  : Ada.Synchronous_Task_Control.Suspension_Object;
   SO_Remote : Ada.Synchronous_Task_Control.Suspension_Object;


   package Message_Containers is new Ada.Containers.Indefinite_Vectors (Index_Type   => Positive,
                                                                       Element_Type => Message_Type);

   ------------
   -- Queue --
   ------------

   protected Queue is
      procedure Push (Message : in     Message_Type);
      entry Pop      (Message :    out Message_Type);
   private
      Not_Empty    : Boolean := False;
      Messages     : Message_Containers.Vector := Message_Containers.Empty_Vector;
   end Queue;

   protected body Queue is
      procedure Push (Message : in     Message_Type)
      is
      begin
         Messages.Append (Message);
         Not_Empty := True;
      end Push;

      entry Pop      (Message :    out Message_Type) when Not_Empty
      is
      begin
         Message := Messages.First_Element;
         Messages.Delete_First;
         Not_Empty := not Messages.Is_Empty;
      end Pop;

   end Queue;

   ------------
   -- Local_Queue_Handler --
   ------------

   task Local_Queue_Handler;

   --  Standard input should be a pipe
   --
   --  (while true; do sleep 3600; done) | ./teamate-client love https://localhost:4141 test 23s3
   --
   task body Local_Queue_Handler
   is
   begin
      Ada.Synchronous_Task_Control.Suspend_Until_True (SO_Local);

      loop
         declare
            Line : constant Unbounded_String := To_Unbounded_String (Ada.Text_IO.Get_Line);
         begin
            if Line /= "" then
               Queue.Push ((Content => Line, Kind => Local));
            end if;
         end;
      end loop;
   exception
      when E : others =>
         Ada.Text_IO.Put_Line ("teamate.client.local.unexpected_error " & Ada.Exceptions.Exception_Information (E));
         Ada.Text_IO.Put_Line (Ada.Exceptions.Exception_Information (E));
         GNAT.OS_Lib.OS_Exit (255);
   end Local_Queue_Handler;

   ------------
   -- Remote_Queue_Handler --
   ------------

   task Remote_Queue_Handler;

   task body Remote_Queue_Handler
   is
   begin
      Ada.Synchronous_Task_Control.Suspend_Until_True (SO_Remote);

      loop
         declare
            Line : constant String := AWS.Client.Extras.Receive (Ws);
         begin
            Queue.Push ((Content => To_Unbounded_String (Line), Kind => Remote));
         end;
      end loop;
   exception
      when E : AWS.Client.Extras.Error_Occured_403 =>
         Ada.Text_IO.Put_Line ("teamate.client.remote.error " & Ada.Exceptions.Exception_Message (E));
         AWS.Client.Close (Ws);
         GNAT.OS_Lib.OS_Exit (03);
      when E : AWS.Client.Extras.Error_Occured_404 =>
         Ada.Text_IO.Put_Line ("teamate.client.remote.error " & Ada.Exceptions.Exception_Message (E));
         AWS.Client.Close (Ws);
         GNAT.OS_Lib.OS_Exit (404);
      when E : AWS.Client.Extras.Error_Occured_409 =>
         Ada.Text_IO.Put_Line ("teamate.client.remote.error " & Ada.Exceptions.Exception_Message (E));
         AWS.Client.Close (Ws);
         GNAT.OS_Lib.OS_Exit (09);
      when E : AWS.Client.Extras.Error_Occured_412 =>
         Ada.Text_IO.Put_Line ("teamate.client.remote.error " & Ada.Exceptions.Exception_Message (E));
         AWS.Client.Close (Ws);
         GNAT.OS_Lib.OS_Exit (12);
      when E : AWS.NET.Socket_Error
         | AWS.Client.Connection_Error =>
         Ada.Text_IO.Put_Line ("teamate.client.remote.network_error " & Ada.Exceptions.Exception_Message (E));
         AWS.Client.Close (Ws);
         GNAT.OS_Lib.OS_Exit (142);
      when E : others =>
         Ada.Text_IO.Put_Line ("teamate.client.remote.unexpected_error " & Ada.Exceptions.Exception_Information (E));
         Ada.Text_IO.Put_Line (Ada.Exceptions.Exception_Information (E));
         AWS.Client.Close (Ws);
         GNAT.OS_Lib.OS_Exit (255);
   end Remote_Queue_Handler;

   procedure Usage
   is
   begin
      Ada.Text_IO.Put_Line ("usage: " & Ada.Command_Line.Command_Name & " <user> https://<server>:<port> <session> <pin> <lead|follow> [--debug]");
      Ada.Text_IO.Put_Line ("example: " & Ada.Command_Line.Command_Name & " love https://adalabs.com:4141 review 1234 follow");
      GNAT.OS_Lib.OS_Exit (100);
   end Usage;

begin

   if Ada.Command_Line.Argument_Count < 5 or else (Ada.Command_Line.Argument (5) not in "lead" | "follow")  then
      Usage;
   else
      if Ada.Command_Line.Argument (Ada.Command_Line.Argument_Count) = "--debug" then
         AWS.Client.Extras.Debug := True;
      end if;

      Ada.Text_IO.Put_Line ("cmd;pid;" & Integer'Image (GNAT.OS_Lib.Pid_To_Integer (GNAT.OS_Lib.Current_Process_Id)));

      AWS.Client.Extras.Handshake (Ws);

      Ada.Synchronous_Task_Control.Set_True (SO_Local);
      Ada.Synchronous_Task_Control.Set_True (SO_Remote);

      loop
         begin
            Queue.Pop (Message);
            if AWS.Client.Extras.Debug then
               case Message.Kind is
                  when Local =>
                     Ada.Text_IO.Put_Line (" >> " & To_String (Message.Content));
                  when Remote =>
                     Ada.Text_IO.Put_Line (" << " & To_String (Message.Content));
               end case;
            end if;
            case Message.Kind is
               when Local =>
                  AWS.Client.Extras.Send (Ws, To_String (Message.Content));
               when Remote =>
                  Ada.Text_IO.Put_Line (To_String (Message.Content));
            end case;
         end;
      end loop;
   end if;

exception
   when E : AWS.Client.Extras.Error_Occured_403 =>
      Ada.Text_IO.Put_Line ("teamate.client.main.error " & Ada.Exceptions.Exception_Message (E));
      AWS.Client.Close (Ws);
      GNAT.OS_Lib.OS_Exit (03);
   when E : AWS.Client.Extras.Error_Occured_404 =>
      Ada.Text_IO.Put_Line ("teamate.client.main.error " & Ada.Exceptions.Exception_Message (E));
      AWS.Client.Close (Ws);
      GNAT.OS_Lib.OS_Exit (404);
   when E : AWS.Client.Extras.Error_Occured_409 =>
      Ada.Text_IO.Put_Line ("teamate.client.main.error " & Ada.Exceptions.Exception_Message (E));
      AWS.Client.Close (Ws);
      GNAT.OS_Lib.OS_Exit (09);
   when E : AWS.Client.Extras.Error_Occured_412 =>
      Ada.Text_IO.Put_Line ("teamate.client.main.error " & Ada.Exceptions.Exception_Message (E));
      AWS.Client.Close (Ws);
      GNAT.OS_Lib.OS_Exit (12);
   when E : AWS.NET.Socket_Error
      | AWS.Client.Connection_Error =>
      Ada.Text_IO.Put_Line ("teamate.client.main.network_error " & Ada.Exceptions.Exception_Message (E));
      AWS.Client.Close (Ws);
      GNAT.OS_Lib.OS_Exit (142);
   when E : others =>
      Ada.Text_IO.Put_Line ("teamate.client.main.unexpected_error " & Ada.Exceptions.Exception_Information (E));
      Ada.Text_IO.Put_Line (Ada.Exceptions.Exception_Information (E));
      AWS.Client.Close (Ws);
      GNAT.OS_Lib.OS_Exit (255);
end AdaLabs.Teamate.Client;
