--                              TEAMATE                                     --
--                                                                          --
--                     Copyright (C) 2018-2020, AdaLabs Ltd                 --
--                                                                          --
--  This library is free software;  you can redistribute it and/or modify   --
--  it under terms of the  GNU General Public License  as published by the  --
--  Free Software  Foundation;  either version 3,  or (at your  option) any --
--  later version. This library is distributed in the hope that it will be  --
--  useful, but WITHOUT ANY WARRANTY;  without even the implied warranty of --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--  You should have received a copy of the GNU General Public License and   --
--  a copy of the GCC Runtime Library Exception along with this program;    --
--  see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see   --
--  <http://www.gnu.org/licenses/>.                                         --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------
with Ada.Command_Line,
     Ada.Unchecked_Conversion,
     Ada.Strings.UTF_Encoding.Strings,
     Ada.Text_IO;

with AWS.Client.HTTP_Utils,
     AWS.Utils,
     AWS.Net.Buffered;

with Interfaces;
with Ada.Strings.Fixed;

package body AWS.Client.Extras is

   type One_Bit     is mod 2 ** 1;
   type Seven_Bit   is mod 2 ** 7;

   subtype Unsigned_16 is Interfaces.Unsigned_16;
   subtype Unsigned_64 is Interfaces.Unsigned_64;

   type Message_Kind_Type is (Fragmented,
                              Final) with Size => 1;
   for Message_Kind_Type use (Fragmented => 16#00#,
                              Final   => 16#01#);

   type Mask_Type is (Unmasked,
                      Masked) with Size => 1;
   for Mask_Type use (Unmasked => 16#00#,
                      Masked   => 16#01#);

   type Op_Code_Type is (Invalid,
                         Text_Frame,
                         Binary_Frame,
                         Reserved_03,
                         Reserved_04,
                         Reserved_05,
                         Reserved_06,
                         Reserved_07,
                         Connection_Close,
                         Ping,
                         Pong,
                         Reserve_11,
                         Reserve_12,
                         Reserve_13,
                         Reserve_14,
                         Reserve_15)
     with Size => 4;

   for Op_Code_Type use (Invalid          => 16#00#,
                         Text_Frame       => 16#01#,
                         Binary_Frame     => 16#02#,
                         Reserved_03      => 16#03#,
                         Reserved_04      => 16#04#,
                         Reserved_05      => 16#05#,
                         Reserved_06      => 16#06#,
                         Reserved_07      => 16#07#,
                         Connection_Close => 16#08#,
                         Ping             => 16#09#,
                         Pong             => 16#0A#,
                         Reserve_11       => 16#0B#,
                         Reserve_12       => 16#0C#,
                         Reserve_13       => 16#0D#,
                         Reserve_14       => 16#0E#,
                         Reserve_15       => 16#0F#);

   type Frame_Header1_Type is record
      FIN            : Message_Kind_Type := Final;
      RSV1           : One_Bit           := 0;
      RSV2           : One_Bit           := 0;
      RSV3           : One_Bit           := 0;
      Op_Code        : Op_Code_Type      := Text_Frame;
   end record with
     Size => 8;

   for Frame_Header1_Type use record
      FIN            at 0 range 07 .. 07;
      RSV1           at 0 range 06 .. 06;
      RSV2           at 0 range 05 .. 05;
      RSV3           at 0 range 04 .. 04;
      Op_Code        at 0 range 00 .. 03;
   end record;

   type Frame_Header2_Type is record
      Mask           : Mask_Type := Unmasked;
      Payload_Length : Seven_Bit := 0;
   end record with
     Size => 8;

   for Frame_Header2_Type use record
      Mask           at 0 range 07 .. 07;
      Payload_Length at 0 range 00 .. 06;
   end record;

   subtype SEA_1 is Ada.Streams.Stream_Element_Array (1 .. 1);
   subtype SEA_2 is Ada.Streams.Stream_Element_Array (1 .. 2);

   subtype SEA_8 is Ada.Streams.Stream_Element_Array (1 .. 8);

   function To_Frame_Header1 is new Ada.Unchecked_Conversion (Source => SEA_1,
                                                              Target => Frame_Header1_Type);
   function To_SEA is new Ada.Unchecked_Conversion (Source => Frame_Header1_Type,
                                                    Target => SEA_1);

   function To_Frame_Header2 is new Ada.Unchecked_Conversion (Source => SEA_1,
                                                              Target => Frame_Header2_Type);

   function To_SEA is new Ada.Unchecked_Conversion (Source => Frame_Header2_Type,
                                                    Target => SEA_1);
   function To_Unsigned_16 is new Ada.Unchecked_Conversion (Source => SEA_2,
                                                            Target => Unsigned_16);
   function To_SEA is new Ada.Unchecked_Conversion (Source => Unsigned_16,
                                                    Target => SEA_2);

   function To_Unsigned_64 is new Ada.Unchecked_Conversion (Source => SEA_8,
                                                            Target => Unsigned_64);

   function To_SEA is new Ada.Unchecked_Conversion (Source => Unsigned_64,
                                                    Target => SEA_8);

   ---------
   -- Network_Byte_Order_To_Host_Endianness --
   ---------

   function Network_Byte_Order_To_Host_Endianness (Source : Stream_Element_Array) return Stream_Element_Array
   is
      R    : Stream_Element_Array  := Source;
      P    : Stream_Element_Offset := R'First;
   begin
      for Index in reverse Source'Range loop
         R (P) := Source (Index);
         P     := P + 1;
      end loop;
      return R;
   end Network_Byte_Order_To_Host_Endianness;

   ---------
   -- Image --
   ---------

   function Image (Header : Frame_Header1_Type) return String
   is
   begin
      return "FIN: " & Message_Kind_Type'Image (Header.FIN) &
        " RSV1:" & One_Bit'Image (Header. RSV1) &
        " RSV2:" & One_Bit'Image (Header.RSV2) &
        " RSV3:" & One_Bit'Image (Header.RSV3) &
        " Op_Code: " & Op_Code_Type'Image (Header.Op_Code);
   end Image;

   ---------
   -- Image --
   ---------

   function Image (Header : Frame_Header2_Type) return String
   is
   begin
      return
        " Mask: " & Mask_Type'Image (Header.Mask) &
        " Payload_Length:" & Seven_Bit'Image (Header.Payload_Length);
   end Image;

   ---------
   -- Log --
   ---------

   procedure Log (Message         : String;
                  Carriage_Return : Boolean := True)
   is
   begin
      if Debug then
         if Carriage_Return then
            Ada.Text_IO.Put_Line (Message);
         else
            Ada.Text_IO.Put (Message);
         end if;
      end if;
   end Log;

   ---------
   -- Receive --
   ---------

   function Receive (Connection : in out HTTP_Connection) return String
   is
      Sock              : constant AWS.Net.Socket_Access := Connection.Socket;
      Last              :          Stream_Element_Offset := 1;
      Result            :          Unbounded_String;
      SEA1              :          SEA_1;
      SEA2              :          SEA_2;
      SEA8              :          SEA_8;
      Header1           :          Frame_Header1_Type;
      Header2           :          Frame_Header2_Type;
      Payload_Length_02 :          Unsigned_16;
      Payload_Length_08 :          Unsigned_64;
   begin

      Sock.Receive (SEA1, Last);
      Header1 := To_Frame_Header1 (SEA1);
      Log ("< " & Image (Header1),Carriage_Return => False);

      Sock.Receive (SEA1, Last);
      Header2 := To_Frame_Header2 (SEA1);
      Log (Image (Header2), Carriage_Return => False);

      if Header2.Payload_Length < 126 then
         declare
            SEA : Ada.Streams.Stream_Element_Array (1 .. Stream_Element_Offset (Header2.Payload_Length));
         begin
            Sock.Receive (SEA, Last);
            case Header1.Op_Code is
               when Text_Frame =>
                  for Index in SEA'Range loop
                     Append (Result, Character'Val(SEA (Index)));
                  end loop;
               when others =>
                  null;
            end case;
         end;
      elsif Header2.Payload_Length = 126 then
         Sock.Receive (SEA2, Last);
         SEA2 := Network_Byte_Order_To_Host_Endianness (SEA2);
         Payload_Length_02 := To_Unsigned_16 (SEA2);
         Log (" Extended payload length:" & Unsigned_16'Image (Payload_Length_02), Carriage_Return => False);
         declare
            SEA : Ada.Streams.Stream_Element_Array (1 .. Stream_Element_Offset (Payload_Length_02));
         begin
            Sock.Receive (SEA, Last);
            case Header1.Op_Code is
               when Text_Frame =>
                  for Index in SEA'Range loop
                     Append (Result, Character'Val(SEA (Index)));
                  end loop;
               when others =>
                  null;
            end case;
         end;
      elsif Header2.Payload_Length = 127 then
         Sock.Receive (SEA8, Last);
         SEA8 := Network_Byte_Order_To_Host_Endianness (SEA8);
         Payload_Length_08 := To_Unsigned_64 (SEA8);
         Log (" Extended payload length:" & Unsigned_64'Image (Payload_Length_08));
         declare
            SEA : Ada.Streams.Stream_Element_Array (1 .. Stream_Element_Offset (Payload_Length_08));
         begin
            Sock.Receive (SEA, Last);
            case Header1.Op_Code is
               when Text_Frame =>
                  for Index in SEA'Range loop
                     Append (Result, Character'Val(SEA (Index)));
                  end loop;
               when others =>
                  null;
            end case;
         end;
      end if;
      Log ("");
      return To_String (Result);
   end Receive;

   ---------
   -- Send_Header --
   ---------

   procedure Send_Header (Sock : AWS.Net.Socket_Type'Class;
                          Data : String)
   is
   begin
      AWS.Net.Buffered.Put_Line (Sock, Data);
      Log ("> " & Data);
   end Send_Header;

   ---------
   -- Send_Header --
   ---------

   procedure Send_Header (Socket : Net.Socket_Type'Class; Headers : AWS.Headers.List) is
   begin
      for J in 1 .. AWS.Headers.Count (Headers) loop
         Send_Header (Socket, AWS.Headers.Get_Line (Headers, J));
      end loop;
   end Send_Header;

   ---------
   -- Send --
   ---------

   procedure Send      (Connection : in out HTTP_Connection;
                        Message    : in     String)
   is
      UTF_String : constant String := Ada.Strings.UTF_Encoding.Strings.Encode (Item          => Message,
                                                                               Output_Scheme => Ada.Strings.UTF_Encoding.UTF_8,
                                                                               Output_BOM    => False);
   begin
      Send (Connection,
            To_Unbounded_String (UTF_String));
   end Send;

   ---------
   -- Send --
   ---------

   procedure Send      (Connection : in out HTTP_Connection;
                        Message    : in     Unbounded_String)
   is
      Sock    : constant AWS.Net.Socket_Access := Connection.Socket;
      Header1 :          Frame_Header1_Type;
      Header2 :          Frame_Header2_Type;
   begin
      if Length (Message) < 126 then
         Header1.FIN            := Final;
         Header1.Op_Code        := Text_Frame;
         Header2.Payload_Length := Seven_Bit (Length (Message));
         Header2.Mask           := Unmasked;

         AWS.Net.Buffered.Write (Sock.all, To_SEA (Header1));
         AWS.Net.Buffered.Write (Sock.all, To_SEA (Header2));

         AWS.Net.Buffered.Put (Sock.all, To_String (Message));
         AWS.Net.Buffered.Flush (Sock.all);

      elsif Length (Message) <= 2**16 then
         Header1.FIN            := Final;
         Header1.Op_Code        := Text_Frame;
         Header2.Payload_Length := 126;
         Header2.Mask           := Unmasked;

         AWS.Net.Buffered.Write (Sock.all, To_SEA (Header1));
         AWS.Net.Buffered.Write (Sock.all, To_SEA (Header2));

         declare
            Length     : constant Unsigned_16 := Unsigned_16 (Ada.Strings.Unbounded.Length (Message));
            Length_SEA : constant SEA_2       := To_SEA (Length);
         begin
            AWS.Net.Buffered.Write (Sock.all, Network_Byte_Order_To_Host_Endianness (Length_SEA));
            AWS.Net.Buffered.Put (Sock.all, To_String (Message));
         end;
         AWS.Net.Buffered.Flush (Sock.all);

      else
         Header1.FIN            := Final;
         Header1.Op_Code        := Text_Frame;
         Header2.Payload_Length := 127;
         Header2.Mask           := Unmasked;

         AWS.Net.Buffered.Write (Sock.all, To_SEA (Header1));
         AWS.Net.Buffered.Write (Sock.all, To_SEA (Header2));

         declare
            Length     : constant Unsigned_64 := Unsigned_64 (Ada.Strings.Unbounded.Length (Message));
            Length_SEA : constant SEA_8       := To_SEA (Length);
         begin
            AWS.Net.Buffered.Write (Sock.all, Network_Byte_Order_To_Host_Endianness (Length_SEA));
            AWS.Net.Buffered.Put (Sock.all, To_String (Message));
         end;
         AWS.Net.Buffered.Flush (Sock.all);

      end if;
      Log ("> " & To_String (Message));

   end Send;

   ---------
   -- Handshake --
   ---------

   procedure Handshake (Connection : in out HTTP_Connection)
   is
      Headers :          AWS.Headers.List;
      User    : constant String := Ada.Command_Line.Argument (1);
      Server  : constant String := Ada.Command_Line.Argument (2);
      Session : constant String := Ada.Command_Line.Argument (3);
      Pin     : constant String := Ada.Command_Line.Argument (4);
      Kind    : constant String := Ada.Command_Line.Argument (5);
   begin
      if Debug then
         AWS.Client.Set_Debug (True);
      end if;

      Headers.Add (Name    => "Connection",
                   Value   => "Upgrade");
      Headers.Add (Name    => "Upgrade",
                   Value   => "Websocket");
      Headers.Add (Name    => "Sec-WebSocket-Key",
                   Value   => AWS.Utils.Random_String (22) & "==");
      Headers.Add (Name    => "Sec-WebSocket-Version",
                   Value   => "13");
      Headers.Add (Name    => "Teamate-Pin",
                   Value   => Pin);
      Headers.Add (Name    => "Teamate-Session",
                   Value   => Session);
      Headers.Add (Name    => "Teamate-Kind",
                   Value   => Kind);
      Headers.Add (Name    => "Teamate-User",
                   Value   => User);

      AWS.Client.Create (Connection  => Connection,
                         Host        => Server,
                         Persistent  => True,
                         Server_Push => False);

      AWS.Client.HTTP_Utils.Connect (Connection);

      --
      --
      --
      declare
         Sock    : constant AWS.Net.Socket_Access := Connection.Socket;
      begin
         Send_Header (Sock.all, "GET " & "/forge/teamate/events/" & Session & ' ' & AWS.HTTP_Version);
         Send_Header (Sock.all, Headers);
         AWS.Net.Buffered.New_Line (Sock.all);
         AWS.Net.Buffered.New_Line (Sock.all);
         declare
            Line : constant String := AWS.Client.Read_Until (Connection => Connection,
                                                             Delimiter  => "=",
                                                             Wait       => True);
         begin
            Log ("< " & Line);
            if Ada.Strings.Fixed.Index (Source  => Line,
                                        Pattern => "HTTP/1.1 403") = 1 then
               Ada.Command_Line.Set_Exit_Status (403);
               raise Error_Occured_403 with  "HTTP/1.1 403 invalid pin";
            elsif Ada.Strings.Fixed.Index (Source  => Line,
                                           Pattern => "HTTP/1.1 404") = 1 then
               Ada.Command_Line.Set_Exit_Status (403);
               raise Error_Occured_404 with  "HTTP/1.1 404 invalid session";
            elsif Ada.Strings.Fixed.Index (Source  => Line,
                                           Pattern => "HTTP/1.1 412") = 1 then
               Ada.Command_Line.Set_Exit_Status (412);
               raise Error_Occured_412 with  "HTTP/1.1 412 session creation quota reached";
            elsif Ada.Strings.Fixed.Index (Source  => Line,
                                           Pattern => "HTTP/1.1 409") = 1 then
               Ada.Command_Line.Set_Exit_Status (409);
               raise Error_Occured_409 with  "HTTP/1.1 409 leader conflict";
            end if;
         end;
      end;

   end Handshake;

end AWS.Client.Extras;
