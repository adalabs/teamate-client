--                              TEAMATE                                     --
--                                                                          --
--                     Copyright (C) 2018-2020, AdaLabs Ltd                 --
--                                                                          --
--  This library is free software;  you can redistribute it and/or modify   --
--  it under terms of the  GNU General Public License  as published by the  --
--  Free Software  Foundation;  either version 3,  or (at your  option) any --
--  later version. This library is distributed in the hope that it will be  --
--  useful, but WITHOUT ANY WARRANTY;  without even the implied warranty of --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--  You should have received a copy of the GNU General Public License and   --
--  a copy of the GCC Runtime Library Exception along with this program;    --
--  see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see   --
--  <http://www.gnu.org/licenses/>.                                         --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------
package AWS.Client.Extras is

   Error_Occured_403 : exception;
   Error_Occured_404 : exception;
   Error_Occured_409 : exception;
   Error_Occured_412 : exception;

   Debug : Boolean := False;

   procedure Handshake (Connection : in out HTTP_Connection);
   function Receive    (Connection : in out HTTP_Connection) return String;
   procedure Send      (Connection : in out HTTP_Connection;
                        Message    : in     Unbounded_String);
   procedure Send      (Connection : in out HTTP_Connection;
                        Message    : in     String);
end AWS.Client.Extras;
